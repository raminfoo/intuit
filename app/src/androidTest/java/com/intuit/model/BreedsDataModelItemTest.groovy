package com.intuit.model

import com.intuit.R

class BreedsDataModelItemTest extends groovy.util.GroovyTestCase {
    void setUp() {
        super.setUp()
    }

    void tearDown() {

    }

    void testGetRating() {
        BreedsDataModelItem dataModelItem = new BreedsDataModelItem()
        dataModelItem.dog_friendly = 1

       int ratingone =  dataModelItem.getRating(dataModelItem.dog_friendly)
       assertEquals(R.drawable.rating_one, ratingone)

    }

    void testFailedGetRating() {
        BreedsDataModelItem dataModelItem = new BreedsDataModelItem()
        dataModelItem.dog_friendly = 5

        int ratingone =  dataModelItem.getRating(dataModelItem.dog_friendly)
        assertFalse(R.drawable.rating_one, ratingone)

    }
}
