package com.intuit.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.intuit.R
import com.intuit.databinding.FragmentBreedsBinding
import com.intuit.model.BreedsDataModelItem
import com.intuit.utils.AppConstant
import com.intuit.utils.ItemViewModel
import com.intuit.utils.State
import com.intuit.view.adapter.BreedsListAdapter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class BreedsFragment : Fragment() {

    var binding: FragmentBreedsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_breeds, container, false
        )
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(!AppConstant.isNetworkAvailable(activity)){
            binding?.progressBar?.visibility = View.GONE
            binding?.tvError?.visibility = View.VISIBLE
            binding?.tvError?.text = getString(R.string.no_internet_string)
            return
        }

        val itemViewModel: ItemViewModel =
            ViewModelProviders.of(this).get(ItemViewModel::class.java)
        binding?.recyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        binding?.recyclerView?.setHasFixedSize(true)
        var adapter  = BreedsListAdapter(requireActivity())
        itemViewModel.itemPagedList.observe(requireActivity(), Observer<PagedList<BreedsDataModelItem>?> { items ->
            adapter.submitList(items)
        })

        itemViewModel.getState().observe(requireActivity(), Observer<State> { items ->
            binding?.progressBar?.visibility = View.GONE

            if(items.equals(State.ERROR)){
                binding?.tvError?.text = getString(R.string.no_internet_string)
            }
        })

        binding?.recyclerView?.adapter = adapter
    }
}