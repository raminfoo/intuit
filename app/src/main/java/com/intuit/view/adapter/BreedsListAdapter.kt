package com.intuit.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.intuit.R
import com.intuit.databinding.AdapterBreedsItemsBinding
import com.intuit.model.BreedsDataModelItem
import com.intuit.utils.AppConstant
import com.intuit.view.ui.BreedDetailsActivity
import com.squareup.picasso.Picasso


class BreedsListAdapter constructor(var mCtx: Context):
    PagedListAdapter<BreedsDataModelItem, BreedsListAdapter.ItemViewHolder>(DIFF_CALLBACK) {

    companion object {
       val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BreedsDataModelItem>() {
            override fun areItemsTheSame(oldItem: BreedsDataModelItem, newItem: BreedsDataModelItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BreedsDataModelItem, newItem: BreedsDataModelItem): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        var breedsItem: AdapterBreedsItemsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(mCtx), R.layout.adapter_breeds_items, parent, false
        )
        return ItemViewHolder(breedsItem)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bindPosition(it, mCtx) }
    }

    class ItemViewHolder(itemView: AdapterBreedsItemsBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        private var binding: AdapterBreedsItemsBinding = itemView
        fun bindPosition(
            breedsItem: BreedsDataModelItem,
            mCtx: Context
        ) {
            if (!TextUtils.isEmpty(breedsItem.image?.url)) {
                Picasso.get()
                    .load(breedsItem.image?.url)
                    .placeholder(R.drawable.default_image)
                    .into(binding.ivImage)
            }
            binding.breedsItems = breedsItem
            binding.cardview.setOnClickListener {
                val options: ActivityOptionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                        mCtx as Activity, binding.ivImage, AppConstant.INTENT_BUNDLE_IMAGE_TRANSITION_NAME
                    )

                var intent = Intent(mCtx, BreedDetailsActivity::class.java)
                intent.putExtra(AppConstant.INTENT_BUNDLE_BREED_OBJECT,breedsItem)
                mCtx.startActivity(intent,  options.toBundle())
            }
        }
    }
}