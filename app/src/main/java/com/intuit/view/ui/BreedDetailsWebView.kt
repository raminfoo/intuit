package com.intuit.view.ui

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.intuit.R
import com.intuit.databinding.ActivityBreedDetailsWebviewBinding
import com.intuit.model.BreedsDataModelItem
import com.intuit.utils.AppConstant


class BreedDetailsWebView : AppCompatActivity() {

    var binding: ActivityBreedDetailsWebviewBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var breedItem = intent.getSerializableExtra(AppConstant.INTENT_BUNDLE_BREED_OBJECT) as BreedsDataModelItem

        binding = DataBindingUtil.setContentView(this, R.layout.activity_breed_details_webview)
        title = breedItem.name
        binding?.breedsItems = breedItem

        binding?.webview?.loadUrl(breedItem.wikipedia_url)
        binding?.webview?.getSettings()?.setJavaScriptEnabled(true)

        binding?.webview?.setWebViewClient(object : WebViewClient() {
            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
            }

            @TargetApi(Build.VERSION_CODES.M)
            override fun onReceivedError(
                view: WebView,
                req: WebResourceRequest,
                rerr: WebResourceError
            ) {
                onReceivedError(
                    view,
                    rerr.errorCode,
                    rerr.description.toString(),
                    req.url.toString()
                )
            }
        })
    }
}