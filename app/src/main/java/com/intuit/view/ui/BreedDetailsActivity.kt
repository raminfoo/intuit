package com.intuit.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.intuit.R
import com.intuit.databinding.ActivityBreedDetailsBinding
import com.intuit.model.BreedsDataModelItem
import com.intuit.utils.AppConstant
import com.squareup.picasso.Picasso

class BreedDetailsActivity : AppCompatActivity() {

    var binding: ActivityBreedDetailsBinding? = null
    var breedItem: BreedsDataModelItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        breedItem = intent.getSerializableExtra(AppConstant.INTENT_BUNDLE_BREED_OBJECT) as BreedsDataModelItem

        binding = DataBindingUtil.setContentView(this, R.layout.activity_breed_details)
        binding?.ivImage?.transitionName = AppConstant.INTENT_BUNDLE_IMAGE_TRANSITION_NAME
        Picasso.get()
            .load(breedItem?.image?.url)
            .placeholder(R.drawable.default_image)
            .into(binding?.ivImage)

        title = breedItem?.name
        binding?.breedsItems = breedItem
    }

    fun openWebView(view: View) {
        var intent = Intent(this, BreedDetailsWebView::class.java)
        intent.putExtra(AppConstant.INTENT_BUNDLE_BREED_OBJECT,breedItem)
        startActivity(intent)
    }
}