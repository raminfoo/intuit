package com.intuit.retrofit

import com.intuit.model.BreedsDataModelItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface APIInterface {

    @GET("breeds")
    fun doGetBreedsList( @Query("page") page: Int,  @Query("limit") limit: Int): Observable<List<BreedsDataModelItem>>
}