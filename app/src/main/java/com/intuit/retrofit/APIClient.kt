package com.intuit.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object APIClient {

    private const val BASE_URL = "https://api.thecatapi.com/v1/"
    var default_timeout = 180


    private var okHttpClient = OkHttpClient.Builder()
        .connectTimeout(default_timeout.toLong(), TimeUnit.SECONDS)
        .readTimeout(default_timeout.toLong(), TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .header("Content-Type", "application/json")
                .build()

            println("URL "+request.url())


            val response = chain.proceed(request)
            response.code()
            response
        }
        .build()

    val instance: APIInterface by lazy{
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()
        retrofit.create(APIInterface::class.java)
    }

}