package com.intuit.utils

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.intuit.model.BreedsDataModelItem

class ItemDataSourceFactory(state: MutableLiveData<State>) :
    DataSource.Factory<Int, BreedsDataModelItem>() {
    val itemLiveDataSource =
        MutableLiveData<PageKeyedDataSource<Int, BreedsDataModelItem>>()

    var state  = state

    override fun create(): DataSource<Int, BreedsDataModelItem> {
        val itemDataSource = ItemDataSource(state) as
                PageKeyedDataSource<Int, BreedsDataModelItem>
        itemLiveDataSource.postValue(itemDataSource)
        return itemDataSource
    }

}