package com.intuit.utils

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.intuit.model.BreedsDataModelItem
import com.intuit.retrofit.APIClient
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ItemDataSource(state:  MutableLiveData<State>) : PageKeyedDataSource<Int?, BreedsDataModelItem>() {


    var state = state

    companion object {
        const val PAGE_SIZE = 10
        private const val FIRST_PAGE = 0
        private const val PER_PAGE_SIZE = 10
    }


    override fun loadInitial(
        params: LoadInitialParams<Int?>,
        callback: LoadInitialCallback<Int?, BreedsDataModelItem>
    ) {
        APIClient.instance.doGetBreedsList(FIRST_PAGE, PER_PAGE_SIZE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<List<BreedsDataModelItem>?>() {
                override fun onCompleted() {
                    Log.d("loadAfter", "onCompleted")
                }

                override fun onError(e: Throwable) {
                    updateState(State.ERROR)
                }

                override fun onNext(response: List<BreedsDataModelItem>?) {
                    if (response != null) {
                        updateState(State.DONE)
                        callback.onResult(response, null, FIRST_PAGE + 1)
                    }

                }
            })
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }


    override fun loadAfter(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, BreedsDataModelItem>
    ) {
        APIClient.instance.doGetBreedsList(params.key, PER_PAGE_SIZE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<List<BreedsDataModelItem>?>() {
                override fun onCompleted() {
                    Log.d("loadAfter", "onCompleted")
                }

                override fun onError(e: Throwable) {
                    updateState(State.ERROR)
                }

                override fun onNext(response: List<BreedsDataModelItem>?) {
                    if (response != null) {
                        updateState(State.DONE)
                        callback.onResult(response, params.key + 1)
                    }
                }
            })
    }

    override fun loadBefore(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, BreedsDataModelItem>
    ) {

    }
}