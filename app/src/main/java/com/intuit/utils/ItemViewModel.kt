package com.intuit.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.intuit.model.BreedsDataModelItem

class ItemViewModel : ViewModel() {
    var itemPagedList: LiveData<PagedList<BreedsDataModelItem>>
    var liveDataSource: LiveData<PageKeyedDataSource<Int, BreedsDataModelItem>>

    var state: MutableLiveData<State> = MutableLiveData()


    init {
        val itemDataSourceFactory = ItemDataSourceFactory(state)
        liveDataSource = itemDataSourceFactory.itemLiveDataSource

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(ItemDataSource.PAGE_SIZE).build()

        itemPagedList = LivePagedListBuilder(itemDataSourceFactory, pagedListConfig)
            .build()
    }


    fun getState(): LiveData<State> {
        return state
    }

}