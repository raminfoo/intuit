package com.intuit.utils

enum class State {
    DONE, LOADING, ERROR
}