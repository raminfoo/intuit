package com.intuit.utils

import android.content.Context
import android.net.ConnectivityManager


class AppConstant {

    companion object{

        const val INTENT_BUNDLE_BREED_OBJECT = "intent_breed_item"
        const val INTENT_BUNDLE_IMAGE_TRANSITION_NAME = "intent_image"


        fun isNetworkAvailable(context: Context?): Boolean {
            return if (context == null) {
                false
            } else {
                try {
                    val ConnectMgr =
                        context.getSystemService("connectivity") as ConnectivityManager
                    if (ConnectMgr == null) {
                        false
                    } else {
                        val NetInfo = ConnectMgr.activeNetworkInfo
                        NetInfo?.isConnected ?: false
                    }
                } catch (var3: SecurityException) {
                    false
                }
            }
        }
    }

}