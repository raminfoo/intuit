package com.intuit.model

import com.intuit.R
import java.io.Serializable

class BreedsDataModelItem : Serializable {
    val adaptability: Int? = null // 5
    val affection_level: Int? = null // 5
    val alt_names: String? = null
    val cfa_url: String? = null // http://cfa.org/Breeds/BreedsAB/Balinese.aspx
    val child_friendly: Int? = null // 4
        get() = getRating(field)

    val country_code: String? = null // US
    val country_codes: String? = null // US
    val description: String? =
        null // The Bambino is a breed of cat that was created as a cross between the Sphynx and the Munchkin breeds. The Bambino cat has short legs, large upright ears, and is usually hairless. They love to be handled and cuddled up on the laps of their family members.
    val dog_friendly: Int? = null // 5
        get() = getRating(field)

    val energy_level: Int? = null // 5
    val experimental: Int? = null // 1
    val grooming: Int? = null // 1
        get() = getRating(field)

    val hairless: Int? = null // 1
    val health_issues: Int? = null // 1
        get() = getRating(field)

    val hypoallergenic: Int? = null // 0
    val id: String? = null // bamb
    val image: Image? = null
    val indoor: Int? = null // 0
    val intelligence: Int? = null // 5
        get() = getRating(field)

    val lap: Int? = null // 1
    val life_span: String? = null // 12 - 14
    val name: String? = null // Bambino
    val natural: Int? = null // 0
    val origin: String? = null // United States
    val rare: Int? = null // 0
    val reference_image_id: String? = null // 5AdhMjeEu
    val rex: Int? = null // 0
    val shedding_level: Int? = null // 1
        get() = getRating(field)

    val short_legs: Int? = null // 1
    val social_needs: Int? = null // 3
    val stranger_friendly: Int? = null // 3
    val suppressed_tail: Int? = null // 0
    val temperament: String? = null // Affectionate, Lively, Friendly, Intelligent
    val vcahospitals_url: String? =
        null // https://vcahospitals.com/know-your-pet/cat-breeds/balinese
    val vetstreet_url: String? = null // http://www.vetstreet.com/cats/balinese
    val vocalisation: Int? = null // 3
    val weight: Weight? = null
    val wikipedia_url: String? = null// https://en.wikipedia.org/wiki/Bambino_cat


    fun getRating(rating: Int?): Int {
        when (rating) {
            5 -> return R.drawable.rating_five
            4 -> return R.drawable.rating_four
            3 -> return R.drawable.rating_three
            2 -> return R.drawable.rating_two
            else -> return R.drawable.rating_one
        }
        return R.drawable.rating_one
    }
}