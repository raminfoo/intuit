package com.intuit.model

import java.io.Serializable

class Weight : Serializable {
    val imperial: String? = null  // 4 - 9
    val metric: String? = null  // 2 - 4
}