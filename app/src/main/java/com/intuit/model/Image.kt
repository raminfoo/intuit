package com.intuit.model

import java.io.Serializable

class Image : Serializable {
    val height: Int? = null  // 1030
    val id: String? = null  // 5AdhMjeEu
    val url: String? = null  // https://cdn2.thecatapi.com/images/5AdhMjeEu.jpg
        get() = field
    val width: Int? = null  // 1296
}